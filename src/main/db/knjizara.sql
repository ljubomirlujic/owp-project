DROP SCHEMA IF EXISTS knjizara;
CREATE SCHEMA knjizara DEFAULT CHARACTER SET utf8;
USE knjizara;

CREATE TABLE korisnici (
	ime VARCHAR(20) NOT NULL,
    prezime VARCHAR(20) NOT NULL,
	korisnickoIme VARCHAR(20),
	lozinka VARCHAR(20) NOT NULL,
	eMail VARCHAR(20) NOT NULL,
    datumR DATE NOT NULL,
    adresa VARCHAR(30) NOT NULL,
    brojTelefona VARCHAR(15) NOT NULL,
    datumIVR DATETIME NOT NULL,
    uloga VARCHAR(15) DEFAULT 'kupac',
	PRIMARY KEY(korisnickoIme)
);

CREATE TABLE knjiga (
	id BIGINT AUTO_INCREMENT,
	naziv VARCHAR(20) NOT NULL,
    isbn BIGINT NOT NULL,
    izdavackaKuca VARCHAR(20) NOT NULL,
    autor VARCHAR(20) NOT NULL,
    godinaIzdavanja INT NOT NULL,
    opis TEXT NOT NULL,
    slika TEXT NOT NULL,
    cena DOUBLE NOT NULL,
    brojStranica INT NOT NULL,
    tipPoveza VARCHAR(5) DEFAULT 'tvrdi',
    pismo VARCHAR(10) DEFAULT 'latinica',
    jezik VARCHAR(20) NOT NULL,
    ocena DOUBLE DEFAULT NULL,
    brojPrimeraka INT,
    PRIMARY KEY (id)
);

CREATE TABLE zanrovi (
	id BIGINT AUTO_INCREMENT,
	ime VARCHAR(20) NOT NULL,
    opis varchar(30) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE KnjigaZanr (
    knjigaId BIGINT,
    zanrId BIGINT,
    PRIMARY KEY(knjigaId, zanrId),
    FOREIGN KEY(knjigaId) REFERENCES knjiga(id)
		ON DELETE CASCADE,
    FOREIGN KEY(zanrId) REFERENCES zanrovi(id)
		ON DELETE CASCADE
);

CREATE TABLE loyalityKartica (
	id BIGINT AUTO_INCREMENT,
	popust FLOAT NOT NULL,
    brojP INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE kupljenaKnjiga (
	id BIGINT AUTO_INCREMENT,
	popust FLOAT NOT NULL,
    brojP INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE kupovina (
	id BIGINT AUTO_INCREMENT,
	ukupnaCena FLOAT NOT NULL,
    datumKupovine DATE NOT NULL,
    brojKnjiga INT NOT NULL,
    korisnik VARCHAR(20) NOT NULL,
    FOREIGN KEY(korisnik) REFERENCES korisnici(korisnickoIme),
    PRIMARY KEY (id)
);

CREATE TABLE komentar (
	id BIGINT AUTO_INCREMENT,
	tekst TEXT NOT NULL,
    ocena FLOAT NOT NULL,
    datum DATE NOT NULL,
    autor VARCHAR(20) NOT NULL,
    FOREIGN KEY(autor) REFERENCES korisnici(korisnickoIme),
    knjiga BIGINT NOT NULL,
    FOREIGN KEY(knjiga) REFERENCES knjiga(id),
    
    PRIMARY KEY (id)
);

CREATE TABLE statusKomentara (
	id BIGINT AUTO_INCREMENT,
	status ENUM('na cekanju','odobren','odbijen') DEFAULT 'na cekanju',
    PRIMARY KEY (id)
);

INSERT INTO korisnici (ime, prezime, korisnickoIme, lozinka, eMail, datumR, adresa, brojTelefona, datumIVR, uloga)
 VALUES ("Petar", "Petrovic", "pero", "123", "pero@gmail.com","2000-05-05", "Novi Sad", "0649874569", "2021-01-20","administrator");
 
INSERT INTO zanrovi (ime, opis) VALUES ("Akcija", "akcione knjige");
INSERT INTO zanrovi (ime, opis) VALUES ("Ljubavni roman", "ljubavni romani");
INSERT INTO zanrovi (ime, opis) VALUES ("racunari", "tehnologija i internet");
INSERT INTO zanrovi (ime, opis) VALUES ("Roman", "romani");
INSERT INTO zanrovi (ime, opis) VALUES ("Ep", "ep");



