package com.ftn.OWPLibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OwpLibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(OwpLibraryApplication.class, args);
	}
	

}
