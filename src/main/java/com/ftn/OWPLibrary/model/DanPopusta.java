package com.ftn.OWPLibrary.model;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

public class DanPopusta {

	private Long id;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate datum;
	private Float popust;
	
	public DanPopusta() {
		super();
	}

	public DanPopusta(Long id, LocalDate datum, Float popust) {
		super();
		this.id = id;
		this.datum = datum;
		this.popust = popust;
	}

	public DanPopusta(LocalDate datum, Float popust) {
		super();
		this.datum = datum;
		this.popust = popust;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getDatum() {
		return datum;
	}

	public void setDatum(LocalDate datum) {
		this.datum = datum;
	}

	public Float getPopust() {
		return popust;
	}

	public void setPopust(Float popust) {
		this.popust = popust;
	}

	@Override
	public String toString() {
		return "DanPopusta [id=" + id + ", datum=" + datum + ", popust=" + popust + "]";
	}
	
	
}
