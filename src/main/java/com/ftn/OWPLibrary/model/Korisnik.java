package com.ftn.OWPLibrary.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;


public class Korisnik {
	
	private String korisnickoIme;
	private String lozinka;
	private String email;
	private String ime;
	private String prezime;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate datumR;
	private String adresa;
	private String brojTelefona;
	private LocalDateTime datumIVR;
	private String uloga;
	private boolean blokiran;
	
	public Korisnik() {
		super();
	}



	public Korisnik(String korisnickoIme, String lozinka, String email, String ime, String prezime, LocalDate datumR,
			String adresa, String brojTelefona, LocalDateTime datumIVR, String uloga, boolean blokiran) {
		super();
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.email = email;
		this.ime = ime;
		this.prezime = prezime;
		this.datumR = datumR;
		this.adresa = adresa;
		this.brojTelefona = brojTelefona;
		this.datumIVR = datumIVR;
		this.uloga = uloga;
		this.blokiran = blokiran;
	}



	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public LocalDate getDatumR() {
		return datumR;
	}

	public void setDatumR(LocalDate datumR) {
		this.datumR = datumR;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}

	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}

	public LocalDateTime getDatumIVR() {
		return datumIVR;
	}

	public void setDatumIVR(LocalDateTime datumIVR) {
		this.datumIVR = datumIVR;
	}

	public String getUloga() {
		return uloga;
	}

	public void setUloga(String uloga) {
		this.uloga = uloga;
	}
	

	public boolean isBlokiran() {
		return blokiran;
	}



	public void setBlokiran(boolean blokiran) {
		this.blokiran = blokiran;
	}



	@Override
	public String toString() {
		return "Korisnik [korisnickoIme=" + korisnickoIme + ", lozinka=" + lozinka + ", email=" + email + ", ime=" + ime
				+ ", prezime=" + prezime + ", datumR=" + datumR + ", adresa=" + adresa + ", brojTelefona="
				+ brojTelefona + ", datumIVR=" + datumIVR + ", uloga=" + uloga + ", blokiran=" + blokiran + "]";
	}
	
	
	

}
