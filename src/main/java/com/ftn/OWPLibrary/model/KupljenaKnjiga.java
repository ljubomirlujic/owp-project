package com.ftn.OWPLibrary.model;

public class KupljenaKnjiga {
	private Long id;
	private Knjiga knjiga;
	private int brojPrimeraka;
	private Double cena;
	private int brisanjeId;
	
	
	public KupljenaKnjiga() {
		super();
	}


	public KupljenaKnjiga(Knjiga knjiga, int brojPrimeraka, Double cena) {
		super();
		this.knjiga = knjiga;
		this.brojPrimeraka = brojPrimeraka;
		this.cena = cena;
	}
	
	


	public KupljenaKnjiga(Long id, Knjiga knjiga, int brojPrimeraka, Double cena) {
		super();
		this.id = id;
		this.knjiga = knjiga;
		this.brojPrimeraka = brojPrimeraka;
		this.cena = cena;
	}


	public Knjiga getKnjiga() {
		return knjiga;
	}


	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}


	public int getBrojPrimeraka() {
		return brojPrimeraka;
	}


	public void setBrojPrimeraka(int brojPrimeraka) {
		this.brojPrimeraka = brojPrimeraka;
	}


	public Double getCena() {
		return cena;
	}


	public void setCena(Double cena) {
		this.cena = cena;
	}
	


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}
	


	public int getBrisanjeId() {
		return brisanjeId;
	}


	public void setBrisanjeId(int brisanjeId) {
		this.brisanjeId = brisanjeId;
	}


	@Override
	public String toString() {
		return "KupljenaKnjiga [knjiga=" + knjiga + ", brojPrimeraka=" + brojPrimeraka + ", cena=" + cena + "]";
	}
	
	
	
	
	

}
