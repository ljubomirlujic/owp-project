package com.ftn.OWPLibrary.model;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

public class Kupovina {
	private Long id;
	private double ukupnaCena;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
	private LocalDateTime datumKupovine;
	private Korisnik korisnik;
	private int brojKnjiga;
	
	private List<KupljenaKnjiga> kupljeneKnjige = new ArrayList<KupljenaKnjiga>();
	
	

	public Kupovina() {
		super();
	}


	public Kupovina(double ukupnaCena, LocalDateTime datumKupovine, Korisnik korisnik, int brojKnjiga) {
		super();
		this.ukupnaCena = ukupnaCena;
		this.datumKupovine = datumKupovine;
		this.korisnik = korisnik;
		this.brojKnjiga = brojKnjiga;
	}
	
	
	
	public Kupovina(Long id, double ukupnaCena, LocalDateTime datumKupovine, Korisnik korisnik, int brojKnjiga) {
		super();
		this.id = id;
		this.ukupnaCena = ukupnaCena;
		this.datumKupovine = datumKupovine;
		this.korisnik = korisnik;
		this.brojKnjiga = brojKnjiga;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public double getUkupnaCena() {
		return ukupnaCena;
	}


	public void setUkupnaCena(double ukupnaCena) {
		this.ukupnaCena = ukupnaCena;
	}


	public LocalDateTime getDatumKupovine() {
		return datumKupovine;
	}


	public void setDatumKupovine(LocalDateTime datumKupovine) {
		this.datumKupovine = datumKupovine;
	}


	public Korisnik getKorisnik() {
		return korisnik;
	}


	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}


	public int getBrojKnjiga() {
		return brojKnjiga;
	}


	public void setBrojKnjiga(int brojKnjiga) {
		this.brojKnjiga = brojKnjiga;
	}


	public List<KupljenaKnjiga> getKupljeneKnjige() {
		return kupljeneKnjige;
	}


	public void setKupljeneKnjige(List<KupljenaKnjiga> kupljeneKnjige) {
		this.kupljeneKnjige = kupljeneKnjige;
	}


	@Override
	public String toString() {
		return "Kupovina [ukupnaCena=" + ukupnaCena + ", datumKupovine=" + datumKupovine + ", korisnik=" + korisnik
				+ ", brojKnjiga=" + brojKnjiga + ", kupljeneKnjige=" + kupljeneKnjige + "]";
	}
	
	
}
