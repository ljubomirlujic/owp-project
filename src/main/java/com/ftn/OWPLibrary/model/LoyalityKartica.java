package com.ftn.OWPLibrary.model;

import com.ftn.OWPLibrary.enumerate.EStatusKartice;

public class LoyalityKartica {

	private Long Id;
	private Double popust;
	private int brojPoena;
	private Korisnik korisnik;
	private EStatusKartice status;
	
	public LoyalityKartica() {
		super();
	}

	public LoyalityKartica(Double popust, int brojPoena, Korisnik korisnik, EStatusKartice status) {
		super();
		this.popust = popust;
		this.brojPoena = brojPoena;
		this.korisnik = korisnik;
		this.status = status;
	}


	public LoyalityKartica(Long id, Double popust, int brojPoena, Korisnik korisnik, EStatusKartice status) {
		super();
		Id = id;
		this.popust = popust;
		this.brojPoena = brojPoena;
		this.korisnik = korisnik;
		this.status = status;
	}

	public Long getId() {
		return Id;
	}


	public void setId(Long id) {
		Id = id;
	}



	public Double getPopust() {
		return popust;
	}



	public void setPopust(Double popust) {
		this.popust = popust;
	}



	public int getBrojPoena() {
		return brojPoena;
	}



	public void setBrojPoena(int brojPoena) {
		this.brojPoena = brojPoena;
	}



	public Korisnik getKorisnik() {
		return korisnik;
	}



	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public EStatusKartice getStatus() {
		return status;
	}

	public void setStatus(EStatusKartice status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "LoyalityKartica [Id=" + Id + ", popust=" + popust + ", brojPoena=" + brojPoena + ", korisnik="
				+ korisnik + ", status=" + status + "]";
	}




	
	
	
}
