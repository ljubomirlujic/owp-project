package com.ftn.OWPLibrary.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotBlank;


public class Knjiga {
	private Long id;
	@NotBlank(message="ne moze biti prazno")
	private String naziv;
	private Long isbn;
	private String izdavackaKuca;
	private String autor;
	private int godinaIzdavanja;
	private String opis;
	private String slika;
	private Double cena;
	private int brojStranica;
	private String tipPoveza;
	private String pismo;
	private String jezik;
	private Double ocena;
	private int brojPrimeraka;
	
	private List<Zanr> zanrovi = new ArrayList<>();
	
	

	public Knjiga() {
		super();
	}

	public Knjiga(Long id, String naziv, Long isbn, String izdavackaKuca, String autor, int godinaIzdavanja,
			String opis, String slika, Double cena, int brojStranica, String tipPoveza, String pismo, String jezik,
			Double ocena, int brojPrimeraka) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.isbn = isbn;
		this.izdavackaKuca = izdavackaKuca;
		this.autor = autor;
		this.godinaIzdavanja = godinaIzdavanja;
		this.opis = opis;
		this.slika = slika;
		this.cena = cena;
		this.brojStranica = brojStranica;
		this.tipPoveza = tipPoveza;
		this.pismo = pismo;
		this.jezik = jezik;
		this.ocena = ocena;
		this.brojPrimeraka = brojPrimeraka;
	}

	public Knjiga(String naziv, Long isbn, String izdavackaKuca, String autor, int godinaIzdavanja, String opis,
			String slika, Double cena, int brojStranica, String tipPoveza, String pismo, String jezik) {
		super();
		this.naziv = naziv;
		this.isbn = isbn;
		this.izdavackaKuca = izdavackaKuca;
		this.autor = autor;
		this.godinaIzdavanja = godinaIzdavanja;
		this.opis = opis;
		this.slika = slika;
		this.cena = cena;
		this.brojStranica = brojStranica;
		this.tipPoveza = tipPoveza;
		this.pismo = pismo;
		this.jezik = jezik;
		this.ocena = 1.0;
		this.brojPrimeraka = 0;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Long getIsbn() {
		return isbn;
	}

	public void setIsbn(Long isbn) {
		this.isbn = isbn;
	}

	public String getIzdavackaKuca() {
		return izdavackaKuca;
	}

	public void setIzdavackaKuca(String izdavackaKuca) {
		this.izdavackaKuca = izdavackaKuca;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getGodinaIzdavanja() {
		return godinaIzdavanja;
	}

	public void setGodinaIzdavanja(int godinaIzdavanja) {
		this.godinaIzdavanja = godinaIzdavanja;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getSlika() {
		return slika;
	}

	public void setSlika(String slika) {
		this.slika = slika;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public int getBrojStranica() {
		return brojStranica;
	}

	public void setBrojStranica(int brojStranica) {
		this.brojStranica = brojStranica;
	}

	public String getTipPoveza() {
		return tipPoveza;
	}

	public void setTipPoveza(String tipPoveza) {
		this.tipPoveza = tipPoveza;
	}

	public String getPismo() {
		return pismo;
	}

	public void setPismo(String pismo) {
		this.pismo = pismo;
	}

	public String getJezik() {
		return jezik;
	}

	public void setJezik(String jezik) {
		this.jezik = jezik;
	}

	public Double getOcena() {
		return ocena;
	}

	public void setOcena(Double ocena) {
		this.ocena = ocena;
	}

	public int getBrojPrimeraka() {
		return brojPrimeraka;
	}

	public void setBrojPrimeraka(int brojPrimeraka) {
		this.brojPrimeraka = brojPrimeraka;
	}

	public List<Zanr> getZanrovi() {
		return zanrovi;
	}

	public void setZanrovi(List<Zanr> zanrovi) {
		this.zanrovi = zanrovi;
	}

	@Override
	public String toString() {
		return "Knjiga [id=" + id + ", naziv=" + naziv + ", isbn=" + isbn + ", izdavackaKuca=" + izdavackaKuca
				+ ", autor=" + autor + ", godinaIzdavanja=" + godinaIzdavanja + ", opis=" + opis + ", slika=" + slika
				+ ", cena=" + cena + ", brojStranica=" + brojStranica + ", tipPoveza=" + tipPoveza + ", pismo=" + pismo
				+ ", jezik=" + jezik + ", ocena=" + ocena + ", brojPrimeraka=" + brojPrimeraka + ", zanrovi="
				+ zanrovi + "]";
	}
	
	
	
	
	
}


