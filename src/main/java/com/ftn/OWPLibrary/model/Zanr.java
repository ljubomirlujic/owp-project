package com.ftn.OWPLibrary.model;


public class Zanr {
	private Long id;
	private String ime;
	private String opis;
	
	public Zanr() {
		super();
	}

	public Zanr(Long id, String ime, String opis) {
		super();
		this.id = id;
		this.ime = ime;
		this.opis = opis;
	}

	public Zanr(String ime, String opis) {
		super();
		this.ime = ime;
		this.opis = opis;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Zanr other = (Zanr) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Zanr [id=" + id + ", ime=" + ime + ", opis=" + opis + "]";
	}
	
	
	
}
