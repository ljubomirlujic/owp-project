package com.ftn.OWPLibrary.model;

import java.util.List;

public class ListaZelja {

	private Long id;
	private Korisnik korisnik;
	private Knjiga knjiga;
	private List<Knjiga> knjige;
	
	
	
	public ListaZelja() {
		super();
	}


	public ListaZelja(Korisnik korisnik, Knjiga knjiga) {
		super();
		this.korisnik = korisnik;
		this.knjiga = knjiga;
	}



	public ListaZelja(Long id, Korisnik korisnik, Knjiga knjiga) {
		super();
		this.id = id;
		this.korisnik = korisnik;
		this.knjiga = knjiga;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Korisnik getKorisnik() {
		return korisnik;
	}


	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}


	public List<Knjiga> getKnjige() {
		return knjige;
	}


	public void setKnjige(List<Knjiga> knjige) {
		this.knjige = knjige;
	}
	


	public Knjiga getKnjiga() {
		return knjiga;
	}


	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}


	@Override
	public String toString() {
		return "ListaZelja [id=" + id + ", korisnik=" + korisnik + ", knjiga=" + knjiga + ", knjige=" + knjige + "]";
	}
	
	
	
	
}
