package com.ftn.OWPLibrary.service;

import java.time.LocalDate;
import java.util.List;

import com.ftn.OWPLibrary.model.DanPopusta;

public interface DaniPopustaService {

	DanPopusta findOne(Long id);
	
	DanPopusta findOneByDate(LocalDate datum);

	List<DanPopusta> findAll();

	int save(DanPopusta danPopusta);
}
