package com.ftn.OWPLibrary.service;

import java.util.List;

import com.ftn.OWPLibrary.model.KupljenaKnjiga;

public interface KupljenaKnjigaService {
	
	KupljenaKnjiga findOne(Long id);

	List<KupljenaKnjiga> findAll();
	
	List<KupljenaKnjiga> findAll(Long idKupovine);

	int save(KupljenaKnjiga knjiga, int kupovinaId);
}
