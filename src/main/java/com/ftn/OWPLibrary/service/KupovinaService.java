package com.ftn.OWPLibrary.service;

import java.util.List;

import com.ftn.OWPLibrary.model.Kupovina;

public interface KupovinaService {
	Kupovina findOne(Long id);
	
	List<Kupovina> findAll();
	
	List<Kupovina> findAll(String korisnickoIme);
	
	Kupovina save(Kupovina kupovina);
}
