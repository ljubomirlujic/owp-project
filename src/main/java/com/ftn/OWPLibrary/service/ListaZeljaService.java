package com.ftn.OWPLibrary.service;

import java.util.List;

import com.ftn.OWPLibrary.model.ListaZelja;

public interface ListaZeljaService {

	ListaZelja findOne(Long id);

	List<ListaZelja> findAll();
	
	List<ListaZelja> findAll(String korisnickoIme);

	int save(ListaZelja lista);
	
	int delete(Long id);
}
