package com.ftn.OWPLibrary.service;

import java.util.List;

import com.ftn.OWPLibrary.model.Knjiga;
import com.ftn.OWPLibrary.model.Zanr;



public interface KnjigaService {

	Knjiga findOne(Long id);
	List<Knjiga> findAll();
	Knjiga save(Knjiga knjiga);
	List<Knjiga> save(List<Knjiga> knjige);
	Knjiga update(Knjiga knjiga);
	List<Knjiga> update(List<Knjiga> knjige);
	Knjiga delete(Long id);
	List<Knjiga> deleteAll(Zanr zanr);
	void delete(List<Long> ids);
	List<Knjiga> find(String naziv, Long zanrId, Double cenaMin, Double cenaMax, String autor, String jezik,Long isbn);
	List<Knjiga> findByZanrId(Long zanrId);
}
