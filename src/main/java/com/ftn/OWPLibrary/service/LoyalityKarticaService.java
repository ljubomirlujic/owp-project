package com.ftn.OWPLibrary.service;

import java.util.List;

import com.ftn.OWPLibrary.enumerate.EStatusKartice;
import com.ftn.OWPLibrary.model.LoyalityKartica;

public interface LoyalityKarticaService {

	LoyalityKartica findOne(Long id);
	
	LoyalityKartica findOne(String korisnickoIme);

	List<LoyalityKartica> findAll();
	
	List<LoyalityKartica> findAll(EStatusKartice status);
	
	int update(LoyalityKartica kartica);

	int save(LoyalityKartica kartica);
}
