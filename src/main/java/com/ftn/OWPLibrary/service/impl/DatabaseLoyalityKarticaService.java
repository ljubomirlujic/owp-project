package com.ftn.OWPLibrary.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPLibrary.dao.LoyalityKarticaDAO;
import com.ftn.OWPLibrary.enumerate.EStatusKartice;
import com.ftn.OWPLibrary.model.LoyalityKartica;
import com.ftn.OWPLibrary.service.LoyalityKarticaService;

@Service
public class DatabaseLoyalityKarticaService implements LoyalityKarticaService {

	@Autowired
	private LoyalityKarticaDAO loyalityKartica;
	
	@Override
	public LoyalityKartica findOne(Long id) {
		return loyalityKartica.findOne(id);
	}

	@Override
	public List<LoyalityKartica> findAll() {
		return loyalityKartica.findAll();
	}

	@Override
	public int save(LoyalityKartica kartica) {
		return loyalityKartica.save(kartica);
	}
	
	@Override
	public int update(LoyalityKartica kartica) {
		return loyalityKartica.update(kartica);
	}

	@Override
	public LoyalityKartica findOne(String korisnickoIme) {
		return loyalityKartica.findOne(korisnickoIme);
	}

	@Override
	public List<LoyalityKartica> findAll(EStatusKartice status) {
		return loyalityKartica.findAll(status);
	}

}
