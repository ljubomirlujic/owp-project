package com.ftn.OWPLibrary.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPLibrary.dao.ListaZeljaDAO;
import com.ftn.OWPLibrary.model.ListaZelja;
import com.ftn.OWPLibrary.service.ListaZeljaService;

@Service
public class DatabaseListaZeljaService implements ListaZeljaService {

	@Autowired
	private ListaZeljaDAO listaZelja;
	@Override
	public ListaZelja findOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ListaZelja> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ListaZelja> findAll(String korisnickoIme) {
		return listaZelja.findAll(korisnickoIme);
	}

	@Override
	public int save(ListaZelja lista) {
		return listaZelja.save(lista);
	}

	@Override
	public int delete(Long id) {
		return listaZelja.delete(id);
	}

}
