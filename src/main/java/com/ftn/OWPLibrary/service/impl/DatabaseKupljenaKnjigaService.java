package com.ftn.OWPLibrary.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPLibrary.dao.KupljenaKnjigaDAO;
import com.ftn.OWPLibrary.model.KupljenaKnjiga;
import com.ftn.OWPLibrary.service.KupljenaKnjigaService;

@Service
public class DatabaseKupljenaKnjigaService implements KupljenaKnjigaService {

	@Autowired
	private KupljenaKnjigaDAO kupljenaKnjiga;
	
	@Override
	public KupljenaKnjiga findOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<KupljenaKnjiga> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int save(KupljenaKnjiga knjiga, int kupovinaId) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<KupljenaKnjiga> findAll(Long idKupovine) {
		return kupljenaKnjiga.findAll(idKupovine);
	}

}
