package com.ftn.OWPLibrary.service.impl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPLibrary.dao.DaniPopustaDAO;
import com.ftn.OWPLibrary.model.DanPopusta;
import com.ftn.OWPLibrary.service.DaniPopustaService;

@Service
public class DatabaseDaniPopustaService implements DaniPopustaService {

	@Autowired
	private DaniPopustaDAO daniPopustaDAO;
	
	@Override
	public DanPopusta findOne(Long id) {
		return daniPopustaDAO.findOne(id);
	}

	@Override
	public List<DanPopusta> findAll() {
		return daniPopustaDAO.findAll();
	}

	@Override
	public int save(DanPopusta danPopusta) {
		return daniPopustaDAO.save(danPopusta);
	}

	@Override
	public DanPopusta findOneByDate(LocalDate datum) {
		return daniPopustaDAO.findOneByDate(datum);
	}

}
