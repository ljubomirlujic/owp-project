package com.ftn.OWPLibrary.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPLibrary.dao.KnjigaDAO;
import com.ftn.OWPLibrary.model.Knjiga;
import com.ftn.OWPLibrary.model.Zanr;
import com.ftn.OWPLibrary.service.KnjigaService;

@Service
public class DatabaseKnjigaService implements KnjigaService {

	@Autowired
	private KnjigaDAO knjigaDAO;

	@Override
	public Knjiga findOne(Long id) {
		return knjigaDAO.findOne(id);
	}

	@Override
	public List<Knjiga> findAll() {
		return knjigaDAO.findAll();
	}

	@Override
	public Knjiga save(Knjiga knjiga) {
		knjigaDAO.save(knjiga);
		return knjiga;
	}

	@Override
	public List<Knjiga> save(List<Knjiga> knjige) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Knjiga update(Knjiga knjiga) {
		knjigaDAO.update(knjiga);
		return knjiga;
	}

	@Override
	public List<Knjiga> update(List<Knjiga> knjige) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Knjiga delete(Long id) {
		Knjiga knjiga = findOne(id);
		if (knjiga != null)
			knjigaDAO.delete(id);
		
		return knjiga;
	}

	@Override
	public List<Knjiga> deleteAll(Zanr zanr) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(List<Long> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Knjiga> find(String naziv, Long zanrId, Double cenaMin, Double cenaMax, String autor, String jezik, Long isbn) {
		return knjigaDAO.find(naziv, zanrId, cenaMin, cenaMax, autor, jezik, isbn);
	}

	@Override
	public List<Knjiga> findByZanrId(Long zanrId) {
		// TODO Auto-generated method stub
		return null;
	}
	


}
