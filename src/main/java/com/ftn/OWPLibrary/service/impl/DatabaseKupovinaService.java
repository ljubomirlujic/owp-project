package com.ftn.OWPLibrary.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.OWPLibrary.dao.impl.KupovinaDAOimpl;
import com.ftn.OWPLibrary.model.Korisnik;
import com.ftn.OWPLibrary.model.Kupovina;
import com.ftn.OWPLibrary.service.KupovinaService;

@Service
public class DatabaseKupovinaService implements KupovinaService{

	@Autowired
	private KupovinaDAOimpl kupovinaDAO;
	
		@Override
		public Kupovina save(Kupovina kupovina) {
		kupovinaDAO.save(kupovina);
		return kupovina;
	}

		@Override
		public Kupovina findOne(Long id) {
			return kupovinaDAO.findOne(id);
		}

		@Override
		public List<Kupovina> findAll() {
			return kupovinaDAO.findAll();
		}

		@Override
		public List<Kupovina> findAll(String korisnickoIme) {
			return kupovinaDAO.findAll(korisnickoIme);
		}
	
}
