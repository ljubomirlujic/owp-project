package com.ftn.OWPLibrary.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileUploadService {

	public void uploadFile(String uploadDir, String fileName, MultipartFile file) throws IllegalStateException, IOException {
		Path uploadPath = Paths.get(uploadDir);
		
		try(InputStream inputStream = file.getInputStream()){
			Path filePath = uploadPath.resolve(fileName);
			Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
		}catch(IOException ioe){
			throw new IOException("Could not save image", ioe);
			
		}
	}
}
