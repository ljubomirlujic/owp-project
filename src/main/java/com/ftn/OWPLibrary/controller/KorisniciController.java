package com.ftn.OWPLibrary.controller;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.OWPLibrary.enumerate.EStatusKartice;
import com.ftn.OWPLibrary.model.Korisnik;
import com.ftn.OWPLibrary.model.Kupovina;
import com.ftn.OWPLibrary.model.ListaZelja;
import com.ftn.OWPLibrary.model.LoyalityKartica;
import com.ftn.OWPLibrary.service.KorisnikService;
import com.ftn.OWPLibrary.service.KupovinaService;
import com.ftn.OWPLibrary.service.ListaZeljaService;
import com.ftn.OWPLibrary.service.LoyalityKarticaService;


@Controller
@RequestMapping(value="Korisnici")
public class KorisniciController {
	
	public static final String KORISNIK_KEY = "prijavljeniKorisnik";
	public static final String KORPA_KEY = "korisnickaKorpa";
	
	@Autowired
	private KorisnikService korisnikService;
	
	@Autowired
	private KupovinaService kupovinaService;
	
	@Autowired
	private ListaZeljaService listaZelja;
	@Autowired
	private LoyalityKarticaService loyalityKartica;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping
	public ModelAndView korisnici(HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null  || !prijavljeniKorisnik.getUloga().equals("administrator")) {
			response.sendRedirect(baseURL);
			return null;
		}
		List<Korisnik> korisnici = korisnikService.findAll();
		
		ModelAndView rezultat = new ModelAndView("korisnici");
		rezultat.addObject("korisnici", korisnici);
		rezultat.addObject("korisnik", prijavljeniKorisnik);



		return rezultat;	
	}
	
	@GetMapping(value="/Profil")
	public ModelAndView profil(HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL);
			return null;
		}
		List<Kupovina> kupovine = kupovinaService.findAll(prijavljeniKorisnik.getKorisnickoIme());
		List<ListaZelja> listaZ = listaZelja.findAll(prijavljeniKorisnik.getKorisnickoIme());
		LoyalityKartica kartica = loyalityKartica.findOne(prijavljeniKorisnik.getKorisnickoIme());

		ModelAndView rezultat = new ModelAndView("profil");
		rezultat.addObject("kupovine", kupovine);
		rezultat.addObject("knjige", listaZ);
		rezultat.addObject("korisnik", prijavljeniKorisnik);

		if(kartica != null && !kartica.getStatus().equals(EStatusKartice.NEODOBRENA)) {

			rezultat.addObject("kartica", kartica);
		}

		return rezultat;	
	}
	
	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam String korisnickoIme,HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null  || !prijavljeniKorisnik.getUloga().equals("administrator")) {
			response.sendRedirect(baseURL);
			return null;
		}
		Korisnik korisnik = korisnikService.findOne(korisnickoIme); 
		List<Kupovina> kupovine = kupovinaService.findAll(korisnik.getKorisnickoIme());
		List<ListaZelja> listaZ = listaZelja.findAll(korisnik.getKorisnickoIme());
		LoyalityKartica kartica = loyalityKartica.findOne(korisnik.getKorisnickoIme());

		ModelAndView rezultat = new ModelAndView("profil");
		rezultat.addObject("kupovine", kupovine);
		rezultat.addObject("knjige", listaZ);
		
		if(!prijavljeniKorisnik.getUloga().equals("administrator")) {
			rezultat.addObject("korisnik", prijavljeniKorisnik);
			
		}
		else {
			rezultat.addObject("korisnik", korisnik);
		}
		if(kartica != null && !kartica.getStatus().equals(EStatusKartice.NEODOBRENA)) {

			rezultat.addObject("kartica", kartica);
		}

		return rezultat;	
	}
	
	@PostMapping(value="/Ogranicenje")
	public String ogranicenje(@RequestParam String korisnickoIme,@RequestParam int status, HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null  || !prijavljeniKorisnik.getUloga().equals("administrator")) {
			response.sendRedirect(baseURL);
			return null;
		}
		Korisnik korisnik = korisnikService.findOne(korisnickoIme); 
		if(korisnik == null) {
			response.sendRedirect(baseURL + "Korisnici");
			return null;
		}
		else {
			if(status == 1) {
			korisnik.setBlokiran(true);
			korisnikService.update(korisnik);
			return "redirect:/Korisnici/Details?korisnickoIme=" + korisnickoIme;	
			}
			else{
				korisnik.setBlokiran(false);
				korisnikService.update(korisnik);
				return "redirect:/Korisnici/Details?korisnickoIme=" + korisnickoIme;	
			}
		}
	}
	

	@PostMapping(value="/Login")
	public ModelAndView postLogin(@RequestParam String korisnickoIme, @RequestParam String lozinka, 
			HttpSession session, HttpServletResponse response) throws IOException {
		try {
			// validacija
			Korisnik korisnik = korisnikService.findOne(korisnickoIme, lozinka);
			if (korisnik == null) {
				throw new Exception("Neispravno korisničko ime ili lozinka!");
			}
			if(korisnik.isBlokiran()) {
				throw new Exception("Onemogucen vam je pristup!");
			}

			// prijava
			session.setAttribute(KorisniciController.KORISNIK_KEY, korisnik);
			Kupovina korpa = new Kupovina();
			session.setAttribute(KorisniciController.KORPA_KEY, korpa);
			response.sendRedirect(baseURL + "Knjige");
			return null;
		} catch (Exception ex) {
			// ispis greške
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešna prijava!";
			}
			
			// prosleđivanje
			ModelAndView rezultat = new ModelAndView("prijava");
			rezultat.addObject("poruka", poruka);

			return rezultat;
		}
	}
	
	@PostMapping(value="/Registracija")
	public ModelAndView register(@ModelAttribute Korisnik kor,@RequestParam String ponovljenaLozinka,
			HttpSession session, HttpServletResponse response) throws IOException {
		try {

			Korisnik postojeciKorisnik = korisnikService.findOne(kor.getKorisnickoIme());
			if (postojeciKorisnik != null) {
				throw new Exception("Korisnicko ime vec postoji!");
			}
			if (kor.getKorisnickoIme().equals("") || kor.getLozinka().equals("")) {
				throw new Exception("Korisnicko ime i lozinka ne smiju biti prazni!");
			}
			if (!kor.getLozinka().equals(ponovljenaLozinka)) {
				throw new Exception("Lozinke se ne podudaraju!");
			}
			if (!kor.getEmail().contains("@") || !kor.getEmail().contains(".")) {
				throw new Exception("Email nije ispravan!");
			}
			if (kor.getIme().equals("") || kor.getPrezime().equals("")) {
				throw new Exception("Ime i prezime ne smiju biti prazni!");
			}
			if (kor.getAdresa().equals("")) {
				throw new Exception("Adresa ne smije biti prazna!");
			}
			if (kor.getBrojTelefona().equals("")) {
				throw new Exception("Broj telefon ne smije biti prazn!");
			}

			LocalDateTime datumRegistracije= LocalDateTime.now();
			kor.setDatumIVR(datumRegistracije);
			Korisnik korisnik = kor;
			korisnikService.save(korisnik);

			response.sendRedirect(baseURL);
			return null;
		} catch (Exception ex) {
			// ispis greške
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Neuspešna registracija!";
			}

			// prosleđivanje
			ModelAndView rezultat = new ModelAndView("registracija");
			rezultat.addObject("poruka", poruka);
			rezultat.addObject("korisnik", kor);
			return rezultat;
		}
	}
	
	
	
	
	@GetMapping(value="/Logout")
	public void logout(HttpSession session, HttpServletResponse response) throws IOException {

		session.invalidate();
		
		response.sendRedirect(baseURL);
	}
}
