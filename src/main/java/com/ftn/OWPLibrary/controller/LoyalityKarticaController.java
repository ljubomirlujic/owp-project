package com.ftn.OWPLibrary.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.OWPLibrary.enumerate.EStatusKartice;
import com.ftn.OWPLibrary.model.Korisnik;
import com.ftn.OWPLibrary.model.Kupovina;
import com.ftn.OWPLibrary.model.LoyalityKartica;
import com.ftn.OWPLibrary.service.LoyalityKarticaService;

@Controller
@RequestMapping(value="Kartica")
public class LoyalityKarticaController {

	@Autowired
	private LoyalityKarticaService loyalityKartica;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping
	public ModelAndView init(HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals("administrator")) {
			response.sendRedirect(baseURL + "Knjige");
			return null;
		}
		List<LoyalityKartica> kartice = loyalityKartica.findAll(EStatusKartice.CEKANJE);
		ModelAndView rezultat = new ModelAndView("loyalityKartice");
		rezultat.addObject("kartice", kartice);

		return rezultat;
	}
	
	@PostMapping(value="/Bonus")
	public ModelAndView bonus(@RequestParam(required=false) Integer brojBodova, HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL);
			return null;
		}
		Kupovina korpa = (Kupovina) session.getAttribute(KorisniciController.KORPA_KEY);
		if(korpa.getKupljeneKnjige().size()< 1) {
			response.sendRedirect(baseURL + "Knjige");
			return null;
		}
		if(brojBodova == null) {
			response.sendRedirect(baseURL + "Korpa");
			return null;
		}
		LoyalityKartica kartica = loyalityKartica.findOne(prijavljeniKorisnik.getKorisnickoIme());
		if(kartica != null && kartica.getStatus().equals(EStatusKartice.ODOBRENA)) {
			if(brojBodova > 10) {
				String poruka = "Maksimalan broj bodova 10!";
				ModelAndView rezultat = new ModelAndView("korpa");
				rezultat.addObject("korpa", korpa);
				rezultat.addObject("kartica", kartica);
				rezultat.addObject("poruka", poruka);
				return rezultat;
			}
			Double bonus = (double) brojBodova * 5;
			Double popust = korpa.getUkupnaCena() * (bonus/100);
			Double cena = korpa.getUkupnaCena() - popust;
			kartica.setBrojPoena(kartica.getBrojPoena() - Integer.valueOf(brojBodova));
			ModelAndView rezultat = new ModelAndView("korpa");
			rezultat.addObject("korpa", korpa);
			rezultat.addObject("cena", cena);
			rezultat.addObject("kartica", kartica);
			rezultat.addObject("bodovi", brojBodova);
			return rezultat;
			}
		else {
			response.sendRedirect(baseURL + "Korpa");
			return null;
		}
	}
	
	@PostMapping(value="/Kreiraj")
	public String create(HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL);
			return null;
		}
		LoyalityKartica pronadjenaKartica = loyalityKartica.findOne(prijavljeniKorisnik.getKorisnickoIme());
		if(pronadjenaKartica != null) {
			pronadjenaKartica.setStatus(EStatusKartice.CEKANJE);
			loyalityKartica.update(pronadjenaKartica);
		}
		else {
			LoyalityKartica novaKartica = new LoyalityKartica(null, 0, prijavljeniKorisnik, EStatusKartice.CEKANJE);
			loyalityKartica.save(novaKartica);
		}
		return "redirect:/Korisnici/Profil";
	}
	@PostMapping(value="/Odobravanje")
	public String odobravanje(@RequestParam Long id,@RequestParam String status ,HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals("administrator")) {
			response.sendRedirect(baseURL);
			return null;
		}
		LoyalityKartica kartica = loyalityKartica.findOne(id);
		if(kartica == null) {
			response.sendRedirect(baseURL + "Kartica");
			return null;
		}

		if(status.equals("1")) {
			kartica.setBrojPoena(4);
			kartica.setStatus(EStatusKartice.ODOBRENA);
			loyalityKartica.update(kartica);
		}
		if(status.equals("0")) {
			kartica.setStatus(EStatusKartice.NEODOBRENA);
			loyalityKartica.update(kartica);
			
		}
		return "redirect:/Kartica";
	}
}
