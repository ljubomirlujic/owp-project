package com.ftn.OWPLibrary.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ftn.OWPLibrary.model.Knjiga;
import com.ftn.OWPLibrary.model.Korisnik;
import com.ftn.OWPLibrary.model.Zanr;
import com.ftn.OWPLibrary.service.KnjigaService;
import com.ftn.OWPLibrary.service.ZanrService;
import com.ftn.OWPLibrary.service.impl.FileUploadService;


@Controller
@RequestMapping(value="/Knjige")
public class KnjigeController {

	@Autowired
	private KnjigaService knjigaService;
	
	@Autowired
	private ZanrService zanrService;
	
	@Autowired
	private FileUploadService fileUpload;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping
	public ModelAndView index(
			@RequestParam(required=false) String naziv,
			@RequestParam(required=false) String jezik, 
			@RequestParam(required=false) String autor,
			@RequestParam(required=false) Long zanrId,
			@RequestParam(required=false) Double cenaOd,
			@RequestParam(required=false) Double cenaDo,
			@RequestParam(required=false) Long isbn,
			HttpSession session)  throws IOException {

		if(naziv!=null && naziv.trim().equals(""))
			naziv=null;
		

		List<Knjiga> knjige = knjigaService.find(naziv, zanrId, cenaOd, cenaDo, autor, jezik,isbn);
		List<Knjiga> knjigeZaPrikaz = new ArrayList<Knjiga>();
		for(Knjiga knjiga : knjige) {
			if (knjiga.getBrojPrimeraka() > 0) {
				knjigeZaPrikaz.add(knjiga);
			}
		}
		List<Zanr> zanrovi = zanrService.findAll();

		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("knjige");
		rezultat.addObject("knjige", knjigeZaPrikaz);
		rezultat.addObject("zanrovi", zanrovi);

		return rezultat;
	}
	
	@GetMapping(value="/Details")
	public ModelAndView details(@RequestParam Long id, 
			HttpSession session, HttpServletRequest request, HttpServletResponse response) throws IOException {
		// čitanje
		Knjiga knjiga = knjigaService.findOne(id);
		List<Zanr> zanrovi = zanrService.findAll();


		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("knjiga");
		rezultat.addObject("knjiga", knjiga);
		rezultat.addObject("zanrovi", zanrovi);

		return rezultat;
	}
	
	@GetMapping(value="/Create")
	public ModelAndView create(HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals("administrator")) {
			response.sendRedirect(baseURL);
			return null;
		}

		List<Zanr> zanrovi = zanrService.findAll();

		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("dodavanjeKnjige");
		rezultat.addObject("zanrovi", zanrovi);

		return rezultat;
	}
	
	
	
	@PostMapping(value="/Create")
	public ModelAndView create(@ModelAttribute Knjiga knj, BindingResult result,
			@RequestParam(name="zanrId", required=false) Long[] zanrIds, 
			@RequestParam MultipartFile file,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals("administrator")) {
			response.sendRedirect(baseURL);
			return null;
		}
		
		List<String> greske = new ArrayList<String>();
		if(knj.getNaziv().trim().length() < 1) {
			String greska = "Naziv ne moze biti prazno polje";	
			greske.add(greska);
		}
		if(knj.getIsbn() == null || (int)Math.log10(knj.getIsbn())+1 != 13) {
			String greska = "ISBN ne moze biti prazno polje(13 cifara)";	
			greske.add(greska);
		}
		
		if(knj.getIzdavackaKuca().trim().length() < 1) {
			String greska = "Izdavacka kuca ne moze biti prazno polje";	
			greske.add(greska);
		}
		
		if(knj.getAutor().trim().length() < 1) {
			String greska = "Autor ne moze biti prazno polje";	
			greske.add(greska);
		}
		
		if(knj.getGodinaIzdavanja() < 1 || knj.getGodinaIzdavanja() > 2021) {
			String greska = "Godina mora biti pozitivan broj";	
			greske.add(greska);
		}
		
		if(knj.getOpis().trim().length() < 1) {
			String greska = "Opis ne moze biti prazno polje";	
			greske.add(greska);
		}
		
		if(knj.getCena() == null || knj.getCena() < 1.0) {
			String greska = "Cena mora biti pozitivan broj";	
			greske.add(greska);
		}
		if(knj.getBrojStranica() < 1) {
			String greska = "Broj stranica mora biti pozitivan broj";	
			greske.add(greska);
		}
		
		
		if(knj.getJezik().trim().length() < 1) {
			String greska = "Jezik ne moze biti prazno polje";	
			greske.add(greska);
		}
		
		if(zanrIds == null || zanrIds.length < 1) {
			String greska = "Odaberite zanr";	
			greske.add(greska);
		}
		
		if(file.isEmpty()) {
			String greska = "Odaberite sliku";	
			greske.add(greska);
		}
		
		
		if(greske.size() > 0) {
			List<Zanr> zanrovi = zanrService.findAll();
			ModelAndView rezultat = new ModelAndView("dodavanje");
			if(zanrIds != null) {
				knj.setZanrovi(zanrService.find(zanrIds));
			}
			rezultat.addObject("knjiga", knj);
			rezultat.addObject("zanrovi", zanrovi);
			rezultat.addObject("greske", greske);
			return rezultat;
		}
		
		 String fileName = file.getOriginalFilename();
		 int startIndex = fileName.replace("\\\\", "/").lastIndexOf("/");
		 fileName = fileName.substring(startIndex +1);
		 String uploadDir = "src/main/resources/static/slike/";
		 fileUpload.uploadFile(uploadDir, fileName, file);
		 
		 
		 knj.setSlika(fileName);

		// kreiranje
		Knjiga knjiga = knj;
		knjiga.setZanrovi(zanrService.find(zanrIds));
		knjigaService.save(knjiga);
		return new ModelAndView("redirect:/Knjige");
	}
	
	@PostMapping(value="/Edit")
	public ModelAndView edit(@RequestParam Long id, @ModelAttribute Knjiga knj, @RequestParam(name="zanrId", required=false) Long[] zanrIds,
			@RequestParam(name="file", required=false) MultipartFile file,
			HttpSession session, HttpServletResponse response) throws IOException {
		// autentikacija, autorizacija
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals("administrator")) {
			response.sendRedirect(baseURL);
		}

		// validacija
		Knjiga knjiga = knjigaService.findOne(id);
		if (knjiga == null) {
			response.sendRedirect(baseURL + "Knjige");
		}	
		List<String> greske = new ArrayList<String>();
		if(knj.getNaziv().trim().length() < 1) {
			String greska = "Naziv ne moze biti prazno polje";	
			greske.add(greska);
		}
		if(knj.getIzdavackaKuca().trim().length() < 1) {
			String greska = "Izdavacka kuca ne moze biti prazno polje";	
			greske.add(greska);
		}
		
		if(knj.getAutor().trim().length() < 1) {
			String greska = "Autor ne moze biti prazno polje";	
			greske.add(greska);
		}
		
		if(knj.getGodinaIzdavanja() < 1 || knj.getGodinaIzdavanja() > 2021) {
			String greska = "Godina mora biti pozitivan broj";	
			greske.add(greska);
		}
		
		if(knj.getOpis().trim().length() < 1) {
			String greska = "Opis ne moze biti prazno polje";	
			greske.add(greska);
		}
		
		if(knj.getCena() == null || knj.getCena() < 1.0) {
			String greska = "Cena mora biti pozitivan broj";	
			greske.add(greska);
		}
		if(knj.getBrojStranica() < 1) {
			String greska = "Broj stranica mora biti pozitivan broj";	
			greske.add(greska);
		}
		
		
		if(knj.getJezik().trim().length() < 1) {
			String greska = "Jezik ne moze biti prazno polje";	
			greske.add(greska);
		}
		
		if(zanrIds == null || zanrIds.length < 1) {
			String greska = "Odaberite zanr";	
			greske.add(greska);
		}
		if(greske.size() > 0) {
			List<Zanr> zanrovi = zanrService.findAll();
			knj.setIsbn(knjiga.getIsbn());
			knj.setBrojPrimeraka(knjiga.getBrojPrimeraka());
			knj.setZanrovi(knjiga.getZanrovi());
			knj.setOcena(knjiga.getOcena());
			ModelAndView rezultat = new ModelAndView("knjiga");
			rezultat.addObject("knjiga", knj);
			rezultat.addObject("zanrovi", zanrovi);
			rezultat.addObject("greske", greske);
			return rezultat;
		}
		

		knjiga.setNaziv(knj.getNaziv());
		knjiga.setIzdavackaKuca(knj.getIzdavackaKuca());
		knjiga.setAutor(knj.getAutor());
		knjiga.setGodinaIzdavanja(knj.getGodinaIzdavanja());
		knjiga.setOpis(knj.getOpis());
		knjiga.setCena(knj.getCena());
		knjiga.setBrojStranica(knj.getBrojStranica());
		knjiga.setTipPoveza(knj.getTipPoveza());
		knjiga.setPismo(knj.getPismo());
		knjiga.setJezik(knj.getJezik());
		knjiga.setOcena(knj.getOcena());
		knjiga.setZanrovi(zanrService.find(zanrIds));

		if(!file.isEmpty()) {
			 String fileName = file.getOriginalFilename();
			 int startIndex = fileName.replace("\\\\", "/").lastIndexOf("/");
			 fileName = fileName.substring(startIndex +1);
			 knjiga.setSlika(fileName);
			 String uploadDir = "src/main/resources/static/slike/";
			 fileUpload.uploadFile(uploadDir, fileName, file);
		}
		knjigaService.update(knjiga);
		response.sendRedirect(baseURL + "Knjige");
		return null;
	}
	
	@PostMapping(value="/Porucivanje")
	public String uploadFile(@RequestParam Long id,@RequestParam Integer brojPrimeraka, HttpSession session, HttpServletResponse response) throws IllegalStateException, IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null || !prijavljeniKorisnik.getUloga().equals("administrator")) {
			response.sendRedirect(baseURL);
		}
		
		Knjiga knjiga = knjigaService.findOne(id);
		if (knjiga == null) {
			response.sendRedirect(baseURL + "Knjige");
		}
		knjiga.setBrojPrimeraka(knjiga.getBrojPrimeraka() +  brojPrimeraka);
		knjigaService.update(knjiga);
		 return "redirect:/Knjige/Details?id=" + id;
	}
	
	@PostMapping(value="/Upload")
	public String uploadFile(@RequestParam("file") MultipartFile file, RedirectAttributes ra) throws IllegalStateException, IOException {
		 String fileName = file.getOriginalFilename();
		 int startIndex = fileName.replace("\\\\", "/").lastIndexOf("/");
		 fileName = fileName.substring(startIndex +1);
		 String uploadDir = "src/main/resources/static/slike/";
		 fileUpload.uploadFile(uploadDir, fileName, file);
		 return "redirect:/Knjige";
	}
	

	
}
