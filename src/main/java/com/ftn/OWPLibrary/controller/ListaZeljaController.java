package com.ftn.OWPLibrary.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ftn.OWPLibrary.model.Knjiga;
import com.ftn.OWPLibrary.model.Korisnik;
import com.ftn.OWPLibrary.model.KupljenaKnjiga;
import com.ftn.OWPLibrary.model.Kupovina;
import com.ftn.OWPLibrary.model.ListaZelja;
import com.ftn.OWPLibrary.service.KnjigaService;
import com.ftn.OWPLibrary.service.KorisnikService;
import com.ftn.OWPLibrary.service.KupovinaService;
import com.ftn.OWPLibrary.service.ListaZeljaService;

@Controller
@RequestMapping(value="ListaZelja")
public class ListaZeljaController {

	@Autowired
	private ListaZeljaService listaZelja;
	
	@Autowired
	private KnjigaService knjigaService;
	
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	
	@PostMapping(value="/Dodaj")
	public String add(@RequestParam Long id,HttpSession session, HttpServletResponse response) throws IllegalStateException, IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL);
		}
		Knjiga knjiga = knjigaService.findOne(id);
		ListaZelja listaZ = new ListaZelja(prijavljeniKorisnik, knjiga);
		listaZelja.save(listaZ);
		 return "redirect:/Knjige/Details?id= " + id;
	}
	
	@PostMapping(value="/Brisanje")
	public String delete(@RequestParam int id,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL);
			return null;
		}
		List<ListaZelja> listaZ = listaZelja.findAll(prijavljeniKorisnik.getKorisnickoIme());

		for(ListaZelja itListaZ : listaZ) {
			if(itListaZ.getKnjiga().getId() == id) {
				listaZ.remove(itListaZ);
				listaZelja.delete(itListaZ.getId());
				break;
			}
		}
		

		return "redirect:/Korisnici/Profil";
	}
	
}
