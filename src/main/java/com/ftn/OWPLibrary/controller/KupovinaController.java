package com.ftn.OWPLibrary.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.OWPLibrary.dao.impl.KupovinaDAOimpl;
import com.ftn.OWPLibrary.enumerate.EStatusKartice;
import com.ftn.OWPLibrary.model.DanPopusta;
import com.ftn.OWPLibrary.model.Knjiga;
import com.ftn.OWPLibrary.model.Korisnik;
import com.ftn.OWPLibrary.model.KupljenaKnjiga;
import com.ftn.OWPLibrary.model.Kupovina;
import com.ftn.OWPLibrary.model.LoyalityKartica;
import com.ftn.OWPLibrary.model.Zanr;
import com.ftn.OWPLibrary.service.DaniPopustaService;
import com.ftn.OWPLibrary.service.KnjigaService;
import com.ftn.OWPLibrary.service.KupljenaKnjigaService;
import com.ftn.OWPLibrary.service.KupovinaService;
import com.ftn.OWPLibrary.service.LoyalityKarticaService;
import com.ftn.OWPLibrary.service.impl.DatabaseKupovinaService;


@Controller
@RequestMapping(value="Korpa")
public class KupovinaController {
	
	@Autowired
	private KnjigaService knjigaService;
	
	@Autowired
	private KupljenaKnjigaService kupljenaKnjigaService;
	
	@Autowired
	private KupovinaService kupovinaService;
	
	@Autowired
	private LoyalityKarticaService loyalityKartica;
	
	@Autowired
	private DaniPopustaService daniPopusta;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping
	public ModelAndView index(HttpSession session, HttpServletResponse response)  throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL);
			return null;
		}
		Kupovina korpa = (Kupovina) session.getAttribute(KorisniciController.KORPA_KEY);
		LoyalityKartica kartica = loyalityKartica.findOne(prijavljeniKorisnik.getKorisnickoIme());
		LocalDate danas = LocalDate.now();
		DanPopusta popust = daniPopusta.findOneByDate(danas);
		Float dnevniPopust = null;
		if(popust != null) {
			dnevniPopust = popust.getPopust();
		}
		
		// prosleđivanje
		ModelAndView rezultat = new ModelAndView("korpa");
		rezultat.addObject("korpa", korpa);
		if(kartica != null  && kartica.getStatus().equals(EStatusKartice.ODOBRENA)) {
			rezultat.addObject("kartica", kartica);
		}
		if(dnevniPopust != null) {
			rezultat.addObject("dnevniPopust", dnevniPopust);
		}
		return rezultat;
	}
	@GetMapping(value = "Details")
	public ModelAndView details(@RequestParam Long id, HttpSession session, HttpServletResponse response)  throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL);
			return null;
		}
		List<KupljenaKnjiga> kupljeneKnjige = kupljenaKnjigaService.findAll(id);
		Kupovina kupovina = kupovinaService.findOne(id);
		ModelAndView rezultat = new ModelAndView("kupovinaInfo");
		rezultat.addObject("kupljeneKnjige", kupljeneKnjige);
		rezultat.addObject("kupovina", kupovina);
		return rezultat;
	}
	
	@PostMapping(value="/Dodavanje")
	public String create(@RequestParam Long knjigaId, @RequestParam int brojPrimeraka,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL);
			return null;
		}
		Random rand = new Random(); 
		Kupovina korpa = (Kupovina) session.getAttribute(KorisniciController.KORPA_KEY);
		Knjiga knjiga = knjigaService.findOne(knjigaId);
		
		
		
		KupljenaKnjiga kupljenaKnjiga = new KupljenaKnjiga(knjiga, brojPrimeraka, knjiga.getCena()*brojPrimeraka);
		kupljenaKnjiga.setBrisanjeId(rand.nextInt(10000));
		
		korpa.getKupljeneKnjige().add(kupljenaKnjiga);
		korpa.setBrojKnjiga(korpa.getBrojKnjiga() + kupljenaKnjiga.getBrojPrimeraka());
		korpa.setUkupnaCena(korpa.getUkupnaCena() + knjiga.getCena()*brojPrimeraka);
		
		return "redirect:/Korpa";
	}
	
	@PostMapping(value="/Brisanje")
	public String delete(@RequestParam int id,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL);
			return null;
		}
		Kupovina korpa = (Kupovina) session.getAttribute(KorisniciController.KORPA_KEY);

		for(KupljenaKnjiga kupljenaK : korpa.getKupljeneKnjige()) {
			if(kupljenaK.getBrisanjeId() == id) {
				korpa.getKupljeneKnjige().remove(kupljenaK);
				korpa.setUkupnaCena(korpa.getUkupnaCena() - kupljenaK.getCena());
				korpa.setBrojKnjiga(korpa.getBrojKnjiga() - kupljenaK.getBrojPrimeraka());
				break;
			}
		}
		

		return "redirect:/Korpa";
	}
	
	@PostMapping(value="/Kupovina")
	public ModelAndView create(@RequestParam(required=false) String brojBodova,@RequestParam(required=false) Double cena,
			@RequestParam(required=false) Float dnevniPopust,
			HttpSession session, HttpServletResponse response) throws IOException {
		
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null) {
			response.sendRedirect(baseURL);
			return null;
		}
		Kupovina korpa = (Kupovina) session.getAttribute(KorisniciController.KORPA_KEY);
		if(korpa.getKupljeneKnjige().size()< 1) {
			response.sendRedirect(baseURL + "Knjige");
			return null;
		}

		korpa.setKorisnik(prijavljeniKorisnik);
		korpa.setDatumKupovine(LocalDateTime.now());
		
		LoyalityKartica kartica = loyalityKartica.findOne(prijavljeniKorisnik.getKorisnickoIme());

		if(cena != null) {
			korpa.setUkupnaCena(cena);
			kupovinaService.save(korpa);
		}
		else if(dnevniPopust != null){
			Double popust = korpa.getUkupnaCena() * (dnevniPopust/100);
			Double ukupnaCena = korpa.getUkupnaCena() - popust;
			korpa.setUkupnaCena(ukupnaCena);
			kupovinaService.save(korpa);		
		}
		else {
			kupovinaService.save(korpa);
		}
		if(kartica != null  && kartica.getStatus().equals(EStatusKartice.ODOBRENA) && dnevniPopust == null) {
			if(brojBodova != "") {
				kartica.setBrojPoena(kartica.getBrojPoena() - Integer.valueOf(brojBodova));
				loyalityKartica.update(kartica);
			}
			kartica.setBrojPoena(kartica.getBrojPoena() + (int)(korpa.getUkupnaCena() / 1000));
			loyalityKartica.update(kartica);
		}
		session.removeAttribute(KorisniciController.KORPA_KEY);
		Kupovina novaKorpa = new Kupovina();
		session.setAttribute(KorisniciController.KORPA_KEY, novaKorpa);
		
		String poruka = "Uspesna kupovina!";
		ModelAndView rezultat = new ModelAndView("korpa");
		rezultat.addObject("korpa", novaKorpa);
		if(kartica != null  && kartica.getStatus().equals(EStatusKartice.ODOBRENA)) {
			rezultat.addObject("kartica", kartica);
		}
		rezultat.addObject("poruka", poruka);
		return rezultat;
		
		}
	


}
