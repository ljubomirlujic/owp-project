package com.ftn.OWPLibrary.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ftn.OWPLibrary.model.DanPopusta;
import com.ftn.OWPLibrary.model.Korisnik;
import com.ftn.OWPLibrary.model.Kupovina;
import com.ftn.OWPLibrary.service.DaniPopustaService;

@Controller
@RequestMapping(value="Popust")
public class DaniPopustaController {

	@Autowired
	private DaniPopustaService daniPopusta;
	
	@Autowired
	private ServletContext servletContext;
	private String baseURL;
	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	} 

	@PostConstruct
	public void init() {
		baseURL = servletContext.getContextPath() + "/";
	}
	
	@GetMapping
	public ModelAndView index(HttpSession session, HttpServletResponse response)  throws IOException {

		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null  || !prijavljeniKorisnik.getUloga().equals("administrator")) {
			response.sendRedirect(baseURL);
			return null;
		}
		
		ModelAndView rezultat = new ModelAndView("danPopusta");

		return rezultat;
	}
	
	@PostMapping(value = "Kreiraj")
	public ModelAndView create(@RequestParam(required=false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate datum, @RequestParam(required=false) Float popust, HttpSession session, HttpServletResponse response)  throws IOException {
		Korisnik prijavljeniKorisnik = (Korisnik) session.getAttribute(KorisniciController.KORISNIK_KEY);
		if (prijavljeniKorisnik == null  || !prijavljeniKorisnik.getUloga().equals("administrator")) {
			response.sendRedirect(baseURL);
			return null;
		}
		try {

			if(datum == null) {
				throw new Exception("Morate odabrati datum!");
			}
			if(popust == null) {
				throw new Exception("Morate uneti popust!");
			}
			DanPopusta danP = daniPopusta.findOneByDate(datum);
			if(danP != null) {
				throw new Exception("Popust za odabrani datum vec posotji!");
			}
			LocalDate danas = LocalDate.now();
			if(datum.isBefore(danas)) {
				throw new Exception("Datum ne moze biti stariji od danasjneg!");
			}
			
			DanPopusta danPopusta = new DanPopusta(datum, popust);
			daniPopusta.save(danPopusta);
			String poruka = "Uspesno kreiran dan popusta";
			ModelAndView rezultat = new ModelAndView("danPopusta");
			rezultat.addObject("poruka", poruka);
			return rezultat;
		} catch (Exception ex) {
			// ispis greške
			String poruka = ex.getMessage();
			if (poruka == "") {
				poruka = "Greska!";
			}
			
			ModelAndView rezultat = new ModelAndView("danPopusta");
			rezultat.addObject("poruka", poruka);

			return rezultat;
		}
	}
}
