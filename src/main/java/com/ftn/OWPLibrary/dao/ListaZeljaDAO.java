package com.ftn.OWPLibrary.dao;

import java.util.List;

import com.ftn.OWPLibrary.model.Korisnik;
import com.ftn.OWPLibrary.model.ListaZelja;

public interface ListaZeljaDAO {

	public ListaZelja findOne(Long id);

	public List<ListaZelja> findAll();
	
	public List<ListaZelja> findAll(String korisnickoIme);

	public int save(ListaZelja lista);
	public int delete(Long id);
}
