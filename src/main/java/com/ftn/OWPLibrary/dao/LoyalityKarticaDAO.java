package com.ftn.OWPLibrary.dao;

import java.util.List;

import com.ftn.OWPLibrary.enumerate.EStatusKartice;
import com.ftn.OWPLibrary.model.LoyalityKartica;



public interface LoyalityKarticaDAO {
	
	public LoyalityKartica findOne(Long id);
	
	public LoyalityKartica findOne(String korisnickoIme);

	public List<LoyalityKartica> findAll();
	public List<LoyalityKartica> findAll(EStatusKartice status);

	public int save(LoyalityKartica kartica);
	public int update(LoyalityKartica kartica);
}
