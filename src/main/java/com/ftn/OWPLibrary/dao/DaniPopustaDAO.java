package com.ftn.OWPLibrary.dao;

import java.time.LocalDate;
import java.util.List;

import com.ftn.OWPLibrary.model.DanPopusta;
import com.ftn.OWPLibrary.model.Knjiga;

public interface DaniPopustaDAO {

	public DanPopusta findOne(Long id);
	
	public DanPopusta findOneByDate(LocalDate datum);

	public List<DanPopusta> findAll();

	public int save(DanPopusta danPopusta);
}
