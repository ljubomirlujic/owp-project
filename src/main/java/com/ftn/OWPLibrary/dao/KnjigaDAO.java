package com.ftn.OWPLibrary.dao;

import java.util.List;

import com.ftn.OWPLibrary.model.Knjiga;



public interface KnjigaDAO {
	
	public Knjiga findOne(Long id);

	public List<Knjiga> findAll();

	public int save(Knjiga knjiga);

	public int update(Knjiga knjiga);

	public int delete(Long id);
	
	public List<Knjiga> find(String naziv, Long zanrId, Double cenaMin, Double cenaMax, String autor, String jezik, Long isbn);
}
