package com.ftn.OWPLibrary.dao;

import java.util.List;

import com.ftn.OWPLibrary.model.Kupovina;



public interface KupovinaDAO {
	
	public Kupovina findOne(Long id);

	public List<Kupovina> findAll();
	
	public List<Kupovina> findAll(String korisnickoIme);

	public int save(Kupovina knjiga);
}
