package com.ftn.OWPLibrary.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ftn.OWPLibrary.dao.ListaZeljaDAO;
import com.ftn.OWPLibrary.model.Knjiga;
import com.ftn.OWPLibrary.model.Korisnik;
import com.ftn.OWPLibrary.model.ListaZelja;
import com.ftn.OWPLibrary.model.Zanr;

@Repository
public class ListaZeljaDAOimpl implements ListaZeljaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private KorisnikDAOimpl korisnikDAO;
	
	@Autowired
	private KnjigaDAOimpl knjigaDAO;
	
	private class ListaZeljaRowMapper implements RowMapper<ListaZelja> {

		@Override
		public ListaZelja mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			Korisnik korisnik = korisnikDAO.findOne(rs.getString(index++));
			Knjiga knjiga = knjigaDAO.findOne(rs.getLong(index++));

			ListaZelja listaZelja = new ListaZelja(id, korisnik, knjiga);
			return listaZelja;
		}

	}
	
	@Override
	public ListaZelja findOne(Long id) {
		String sql = "SELECT * FROM listaZelja WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new ListaZeljaRowMapper(), id);
	}

	@Override
	public List<ListaZelja> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int save(ListaZelja lista) {
		String sql = "INSERT INTO listaZelja (korisnik, knjigaId) VALUES (?, ?)";
		return jdbcTemplate.update(sql, lista.getKorisnik().getKorisnickoIme(), lista.getKnjiga().getId());
	}

	@Override
	public List<ListaZelja> findAll(String korisnickoIme) {
		String sql = "SELECT * FROM listaZelja WHERE korisnik = ?";
		return jdbcTemplate.query(sql, new ListaZeljaRowMapper(), korisnickoIme);
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM listaZelja WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}

}
