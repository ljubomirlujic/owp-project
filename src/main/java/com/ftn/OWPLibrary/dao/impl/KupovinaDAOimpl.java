package com.ftn.OWPLibrary.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.OWPLibrary.dao.KupovinaDAO;
import com.ftn.OWPLibrary.model.Knjiga;
import com.ftn.OWPLibrary.model.Korisnik;
import com.ftn.OWPLibrary.model.KupljenaKnjiga;
import com.ftn.OWPLibrary.model.Kupovina;
import com.ftn.OWPLibrary.model.Zanr;


@Repository
public class KupovinaDAOimpl implements KupovinaDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private KorisnikDAOimpl korisnikDAO;
	@Autowired
	private KnjigaDAOimpl knjigaDAO;
	
	@Autowired
	private KupljenaKnjigaDAOimpl kupljenaKnjigaDAO;
	
	private class KupovinaRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, Kupovina> kupovine = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			
			Long kupovinaId = resultSet.getLong(index++);
			Double ukupnaCena = resultSet.getDouble(index++);
			LocalDateTime datumKupovine = resultSet.getTimestamp(index++).toLocalDateTime();
			Integer brojKnjiga = resultSet.getInt(index++);
			Korisnik korisnik = korisnikDAO.findOne(resultSet.getString(index++));


			Kupovina kupovina = kupovine.get(kupovinaId);
			if (kupovina == null) {
				kupovina = new Kupovina(kupovinaId, ukupnaCena, datumKupovine, korisnik, brojKnjiga);
				kupovine.put(kupovinaId, kupovina);
			}

			Long id = resultSet.getLong(index++);
			Knjiga kupljenaKnjiga = knjigaDAO.findOne(resultSet.getLong(index++));
			Integer brojPrimeraka = resultSet.getInt(index++);
			Double cena = resultSet.getDouble(index++);
			
			KupljenaKnjiga knjiga = new KupljenaKnjiga(id, kupljenaKnjiga, brojPrimeraka, cena);
			kupovina.getKupljeneKnjige().add(knjiga);
		}

		public List<Kupovina> getKupovina() {
			return new ArrayList<>(kupovine.values());
		}

	}

	@Override
	public Kupovina findOne(Long id) {
		String sql = 
				"SELECT k.*, knj.* FROM kupovina k " + 
				"INNER JOIN kupljenaKnjiga knj ON k.id = knj.kupovinaId " +  
				"WHERE k.id = ?";

		KupovinaRowCallBackHandler rowCallbackHandler = new KupovinaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getKupovina().get(0);
	}

	@Override
	public List<Kupovina> findAll() {
		String sql = 
				"SELECT k.*, knj.* FROM kupovina k " + 
				"INNER JOIN kupljenaKnjiga knj ON k.id = knj.kupovinaId " +  
				"ORDER BY k.datumKupovine DESC";

		KupovinaRowCallBackHandler rowCallbackHandler = new KupovinaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getKupovina();
	}
	@Transactional
	@Override
	public int save(Kupovina kupovina) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO kupovina (ukupnaCena, datumKupovine, brojKnjiga, korisnik)"
						+ " VALUES (?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setDouble(index++, kupovina.getUkupnaCena());
				preparedStatement.setTimestamp(index++, Timestamp.valueOf(kupovina.getDatumKupovine()));
				preparedStatement.setInt(index++, kupovina.getBrojKnjiga());
				preparedStatement.setString(index++, kupovina.getKorisnik().getKorisnickoIme());
				
				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		if (uspeh) {
			for (KupljenaKnjiga itKnjiga: kupovina.getKupljeneKnjige()) {	
				uspeh = uspeh && kupljenaKnjigaDAO.save(itKnjiga, keyHolder.getKey().intValue()) == 1;
			}
		}
		return uspeh?1:0;
	}

	@Override
	public List<Kupovina> findAll(String korisnickoIme) {
		String sql = 
				"SELECT k.*, knj.* FROM kupovina k " + 
				"INNER JOIN kupljenaKnjiga knj ON k.id = knj.kupovinaId WHERE k.korisnik = ?" +  
				"ORDER BY k.datumKupovine DESC";

		KupovinaRowCallBackHandler rowCallbackHandler = new KupovinaRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, korisnickoIme);

		return rowCallbackHandler.getKupovina();
	}

}
