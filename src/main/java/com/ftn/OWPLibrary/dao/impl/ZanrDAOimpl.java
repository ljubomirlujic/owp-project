package com.ftn.OWPLibrary.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ftn.OWPLibrary.dao.ZanrDAO;
import com.ftn.OWPLibrary.model.Zanr;



@Repository
public class ZanrDAOimpl implements ZanrDAO {

	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class ZanrRowMapper implements RowMapper<Zanr> {

		@Override
		public Zanr mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long id = rs.getLong(index++);
			String ime = rs.getString(index++);
			String opis = rs.getString(index++);

			Zanr zanr = new Zanr(id, ime, opis);
			return zanr;
		}

	}
	@Override
	public Zanr findOne(Long id) {
		String sql = "SELECT id, ime, opis FROM zanrovi WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new ZanrRowMapper(), id);
	}

	@Override
	public List<Zanr> findAll() {
		String sql = "SELECT id, ime, opis FROM zanrovi";
		return jdbcTemplate.query(sql, new ZanrRowMapper());
	}

	@Override
	public List<Zanr> find(String ime) {
		ime = "%" + ime + "%";
		String sql = "SELECT id, ime, opis FROM zanrovi WHERE ime LIKE ?";
		return jdbcTemplate.query(sql, new ZanrRowMapper(), ime);
	}

	@Override
	public int save(Zanr zanr) {
		String sql = "INSERT INTO zanrovi (ime, opis) VALUES (?, ?)";
		return jdbcTemplate.update(sql, zanr.getIme(), zanr.getOpis());
	}

	@Override
	public int[] save(ArrayList<Zanr> zanrovi) {
		String sql = "INSERT INTO zanrovi (ime, opis) VALUES (?, ?)";
		
		return jdbcTemplate.batchUpdate(sql,
				new BatchPreparedStatementSetter() {
					
					@Override
					public void setValues(PreparedStatement ps, int i) throws SQLException {
						ps.setString(1, zanrovi.get(i).getIme());
						ps.setString(2, zanrovi.get(i).getOpis());
					}
					
					@Override
					public int getBatchSize() {
						return zanrovi.size();
					}
				});
	}

	@Override
	public int update(Zanr zanr) {
		String sql = "UPDATE zanrovi SET ime = ?, opis = ? WHERE id = ?";
		return jdbcTemplate.update(sql, zanr.getIme(), zanr.getOpis(), zanr.getId());
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM zanrovi WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}
}
