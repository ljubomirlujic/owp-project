package com.ftn.OWPLibrary.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import com.ftn.OWPLibrary.dao.DaniPopustaDAO;
import com.ftn.OWPLibrary.model.DanPopusta;

@Service
public class DaniPopustaDAOimpl implements DaniPopustaDAO{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class DaniPopustaRowMapper implements RowMapper<DanPopusta> {

		@Override
		public DanPopusta mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;	
			
			Long DanPopustaId = rs.getLong(index++);
			LocalDate datum = rs.getDate(index++).toLocalDate();
			Float popust = rs.getFloat(index++);
			
			DanPopusta danPopusta = new DanPopusta(DanPopustaId, datum, popust);
			return danPopusta;
		}

	}
	
	@Override
	public DanPopusta findOne(Long id) {
		try {
			String sql = "SELECT * FROM daniPopusta WHERE id = ?";
			return jdbcTemplate.queryForObject(sql, new DaniPopustaRowMapper(), id);
		} catch (EmptyResultDataAccessException ex) {

			return null;
		}
	}

	@Override
	public List<DanPopusta> findAll() {
		String sql = "SELECT * FROM daniPopusta";
		return jdbcTemplate.query(sql, new DaniPopustaRowMapper());
	}

	@Override
	public int save(DanPopusta danPopusta) {
		String sql = "INSERT INTO daniPopusta (datum, popust) VALUES (?,?)";
		return jdbcTemplate.update(sql, danPopusta.getDatum(), danPopusta.getPopust());
	}

	@Override
	public DanPopusta findOneByDate(LocalDate datum) {
		try {
			String sql = "SELECT * FROM daniPopusta WHERE datum = ?";
			return jdbcTemplate.queryForObject(sql, new DaniPopustaRowMapper(), datum);
		} catch (EmptyResultDataAccessException ex) {

			return null;
		}
	}

}
