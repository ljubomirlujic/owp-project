package com.ftn.OWPLibrary.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ftn.OWPLibrary.dao.KorisnikDAO;
import com.ftn.OWPLibrary.model.Korisnik;



@Repository
public class KorisnikDAOimpl implements KorisnikDAO {

	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	private class KorisnikRowMapper implements RowMapper<Korisnik> {

		@Override
		public Korisnik mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;	
			String ime = rs.getString(index++);
			String prezime = rs.getString(index++);
			String korisnickoIme = rs.getString(index++);
			String lozinka = rs.getString(index++);
			String email = rs.getString(index++);
			LocalDate datumR = rs.getDate(index++).toLocalDate();
			String adresa = rs.getString(index++);
			String brojTelefona = rs.getString(index++);
			LocalDateTime datumIVR = rs.getTimestamp(index++).toLocalDateTime();
			String uloga = rs.getString(index++);
			Boolean blokiran = rs.getBoolean(index++);

			Korisnik korisnik = new Korisnik(korisnickoIme, lozinka, email, ime, prezime, datumR, adresa, brojTelefona, datumIVR, uloga, blokiran);
			return korisnik;
		}

	}
	
	@Override
	public Korisnik findOne(String korisnickoIme) {
		try {
			String sql = "SELECT * FROM korisnici WHERE korisnickoIme = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}

	@Override
	public Korisnik findOne(String korisnickoIme, String lozinka) {
		try {
			String sql = "SELECT * FROM korisnici WHERE korisnickoIme = ? AND lozinka = ?";
			return jdbcTemplate.queryForObject(sql, new KorisnikRowMapper(), korisnickoIme, lozinka);
		} catch (EmptyResultDataAccessException ex) {
			// ako korisnik nije pronađen
			return null;
		}
	}

	@Override
	public List<Korisnik> findAll() {
		String sql = "SELECT * FROM korisnici";
		return jdbcTemplate.query(sql, new KorisnikRowMapper());
	}

	@Override
	public List<Korisnik> find(String korisnickoIme, String eMail, String pol, Boolean administrator) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Korisnik korisnik) {
		String sql = "INSERT INTO korisnici (ime, prezime, korisnickoIme, lozinka, eMail, datumR, adresa, brojTelefona, datumIVR,blokiran) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
		jdbcTemplate.update(sql, korisnik.getIme(), korisnik.getPrezime(), korisnik.getKorisnickoIme(), korisnik.getLozinka(),
				korisnik.getEmail(), korisnik.getDatumR(), korisnik.getAdresa(), korisnik.getBrojTelefona(), korisnik.getDatumIVR(), korisnik.isBlokiran());

	}

	@Override
	public void update(Korisnik korisnik) {
		if (korisnik.getLozinka() == null) {
			String sql = "UPDATE korisnici SET ime = ?, prezime = ?, korisnickoIme = ?, eMail = ?, datumR = ?, adresa = ?, brojTelefona = ?,"
					+ " datumIVR = ?, blokiran = ? WHERE korisnickoIme = ?";
			jdbcTemplate.update(sql, korisnik.getIme(), korisnik.getPrezime(), korisnik.getKorisnickoIme(),
					korisnik.getEmail(), korisnik.getDatumR(), korisnik.getAdresa(), korisnik.getBrojTelefona(), korisnik.getDatumIVR(), korisnik.isBlokiran() ,korisnik.getKorisnickoIme());
		} else {
			String sql = "UPDATE korisnici SET ime = ?, prezime = ?, korisnickoIme = ?, lozinka = ?, eMail = ?, datumR = ?, adresa = ?, brojTelefona = ?,"
					+ " datumIVR = ?, blokiran = ? WHERE korisnickoIme = ?";
			jdbcTemplate.update(sql, korisnik.getIme(), korisnik.getPrezime(), korisnik.getKorisnickoIme(), korisnik.getLozinka(),
					korisnik.getEmail(), korisnik.getDatumR(), korisnik.getAdresa(), korisnik.getBrojTelefona(), korisnik.getDatumIVR(), korisnik.isBlokiran(),korisnik.getKorisnickoIme());
		}

	}

	@Override
	public void delete(String korisnickoIme) {
		String sql = "DELETE FROM korisnici WHERE korisnickoIme = ?";
		jdbcTemplate.update(sql, korisnickoIme);

	}

}
