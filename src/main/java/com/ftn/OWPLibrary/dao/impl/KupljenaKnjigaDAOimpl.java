package com.ftn.OWPLibrary.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ftn.OWPLibrary.dao.KupljenaKnjigaDAO;
import com.ftn.OWPLibrary.model.Knjiga;
import com.ftn.OWPLibrary.model.KupljenaKnjiga;
import com.ftn.OWPLibrary.service.KnjigaService;
@Repository
public class KupljenaKnjigaDAOimpl implements KupljenaKnjigaDAO {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private KnjigaService knjigaService;
	
	private class KupljenaKnjigaRowMapper implements RowMapper<KupljenaKnjiga> {

		@Override
		public KupljenaKnjiga mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;	
			
			Long kupljenaKnjigaId = rs.getLong(index++);
			Knjiga knjiga = knjigaService.findOne(rs.getLong(index++));
			Integer brojPrimeraka = rs.getInt(index++);
			Double cena = rs.getDouble(index++);
			
			KupljenaKnjiga kupljenaKnjiga = new KupljenaKnjiga(kupljenaKnjigaId, knjiga, brojPrimeraka, cena);
			return kupljenaKnjiga;
		}

	}

	@Override
	public KupljenaKnjiga findOne(Long id) {
		String sql = "SELECT * FROM kupljenaKnjiga where id = ?";
		return jdbcTemplate.queryForObject(sql, new KupljenaKnjigaRowMapper(), id);
	}

	@Override
	public List<KupljenaKnjiga> findAll() {
		String sql = "SELECT * FROM kupljenaKnjiga";
		return jdbcTemplate.query(sql, new KupljenaKnjigaRowMapper());
	}

	@Override
	public int save(KupljenaKnjiga knjiga, int kupovinaId) {
		String sql = "INSERT INTO kupljenaKnjiga (knjigaId, brojPrimeraka, cena, kupovinaId) VALUES (?, ?, ?, ?)";
		return jdbcTemplate.update(sql, knjiga.getKnjiga().getId(), knjiga.getBrojPrimeraka(), knjiga.getCena(), kupovinaId);
		
	}

	@Override
	public List<KupljenaKnjiga> findAll(Long idKupovine) {
		String sql = "SELECT * FROM kupljenaKnjiga WHERE kupovinaId = ?";
		return jdbcTemplate.query(sql, new KupljenaKnjigaRowMapper(), idKupovine);
	}

}
