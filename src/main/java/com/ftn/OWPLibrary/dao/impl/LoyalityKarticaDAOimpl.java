package com.ftn.OWPLibrary.dao.impl;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ftn.OWPLibrary.dao.LoyalityKarticaDAO;
import com.ftn.OWPLibrary.enumerate.EStatusKartice;
import com.ftn.OWPLibrary.model.Korisnik;
import com.ftn.OWPLibrary.model.LoyalityKartica;

@Repository
public class LoyalityKarticaDAOimpl implements LoyalityKarticaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private KorisnikDAOimpl korisnikDAO;
	
	private class LoyalityKarticaRowMapper implements RowMapper<LoyalityKartica> {

		@Override
		public LoyalityKartica mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;	
			
			Long karticaId = rs.getLong(index++);
			Double popust = rs.getDouble(index++);
			Integer brojPoena = rs.getInt(index++);
			Korisnik korisnik = korisnikDAO.findOne(rs.getString(index++));
			EStatusKartice status = EStatusKartice.valueOf(rs.getString(index++));
			
			LoyalityKartica kartica = new LoyalityKartica(karticaId, popust, brojPoena, korisnik, status);
			return kartica;
		}

	}
	
	@Override
	public LoyalityKartica findOne(Long id) {
		String sql = "SELECT * FROM loyalityKartica where id = ?";
		return jdbcTemplate.queryForObject(sql, new LoyalityKarticaRowMapper(), id);
	}

	@Override
	public List<LoyalityKartica> findAll() {
		String sql = "SELECT * FROM loyalityKartica";
		return jdbcTemplate.query(sql, new LoyalityKarticaRowMapper());
	}

	@Override
	public int save(LoyalityKartica kartica) {
		String sql = "INSERT INTO loyalityKartica (popust, brojP, korisnik, status) VALUES (?, ?, ?, ?)";
		return jdbcTemplate.update(sql, kartica.getPopust(), kartica.getBrojPoena(), kartica.getKorisnik().getKorisnickoIme(), kartica.getStatus().toString());
	}
	@Override
	public int update(LoyalityKartica kartica) {
		String sql = "UPDATE loyalityKartica SET popust = ?, brojP = ?, korisnik = ?, status = ? where id = ?";
		return jdbcTemplate.update(sql, kartica.getPopust(), kartica.getBrojPoena(), kartica.getKorisnik().getKorisnickoIme(), kartica.getStatus().toString(), kartica.getId());
	}

	@Override
	public LoyalityKartica findOne(String korisnickoIme) {
			try {
			String sql = "SELECT * FROM loyalityKartica where korisnik = ?";
			return jdbcTemplate.queryForObject(sql, new LoyalityKarticaRowMapper(), korisnickoIme);
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}

	}

	@Override
	public List<LoyalityKartica> findAll(EStatusKartice status) {
		String sql = "SELECT * FROM loyalityKartica WHERE status = ?";
		return jdbcTemplate.query(sql, new LoyalityKarticaRowMapper(), status.toString());
	}

}
