package com.ftn.OWPLibrary.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ftn.OWPLibrary.dao.KnjigaDAO;
import com.ftn.OWPLibrary.dao.ZanrDAO;
import com.ftn.OWPLibrary.model.Knjiga;
import com.ftn.OWPLibrary.model.Zanr;


@Repository
public class KnjigaDAOimpl implements KnjigaDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
	private ZanrDAO zanrDAO;
	

	private class FilmZanrRowCallBackHandler implements RowCallbackHandler {

		private Map<Long, Knjiga> knjige = new LinkedHashMap<>();
		
		@Override
		public void processRow(ResultSet resultSet) throws SQLException {
			int index = 1;
			
			Long knjigaId = resultSet.getLong(index++);
			String knjigaNaziv = resultSet.getString(index++);
			Long knjigaIsbn = resultSet.getLong(index++);
			String knjigaIzdavackaKuca = resultSet.getString(index++);
			String knjigaAutor = resultSet.getString(index++);
			int knjigaGodinaIzdavanja = resultSet.getInt(index++);
			String knjigaOpis = resultSet.getString(index++);
			String knjigaSlika = resultSet.getString(index++);
			Double knjigaCena = resultSet.getDouble(index++);
			int knjigaBrojStranica = resultSet.getInt(index++);
			String knjigaTipPoveza = resultSet.getString(index++);
			String knjigaPismo = resultSet.getString(index++);
			String knjigaJezik = resultSet.getString(index++);
			Double knjigaOcena = resultSet.getDouble(index++);
			Integer brojPrimeraka = resultSet.getInt(index++);

			Knjiga knjiga = knjige.get(knjigaId);
			if (knjiga == null) {
				knjiga = new Knjiga(knjigaId, knjigaNaziv, knjigaIsbn, knjigaIzdavackaKuca, knjigaAutor, knjigaGodinaIzdavanja,
						knjigaOpis, knjigaSlika, knjigaCena, knjigaBrojStranica, knjigaTipPoveza, knjigaPismo, knjigaJezik, knjigaOcena,brojPrimeraka);
				knjige.put(knjiga.getId(), knjiga);
			}

			Long zanrId = resultSet.getLong(index++);
			String zanrIme = resultSet.getString(index++);
			String zanrOpis = resultSet.getString(index++);
			Zanr zanr = new Zanr(zanrId, zanrIme, zanrOpis);
			knjiga.getZanrovi().add(zanr);
		}

		public List<Knjiga> getKnjige() {
			return new ArrayList<>(knjige.values());
		}

	}
	
	
	@Override
	public Knjiga findOne(Long id) {
		String sql = 
				"SELECT k.*, z.* FROM knjiga k " + 
				"LEFT JOIN knjigaZanr kz ON kz.knjigaId = k.id " + 
				"LEFT JOIN zanrovi z ON kz.zanrId = z.id " + 
				"WHERE k.id = ? " + 
				"ORDER BY k.id";

		FilmZanrRowCallBackHandler rowCallbackHandler = new FilmZanrRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler, id);

		return rowCallbackHandler.getKnjige().get(0);
	}

	@Override
	public List<Knjiga> findAll() {
		String sql = 
				"SELECT k.*, z.* FROM knjiga k " + 
				"LEFT JOIN knjigaZanr kz ON kz.knjigaId = k.id " + 
				"LEFT JOIN zanrovi z ON kz.zanrId = z.id " + 
				"ORDER BY k.id";

		FilmZanrRowCallBackHandler rowCallbackHandler = new FilmZanrRowCallBackHandler();
		jdbcTemplate.query(sql, rowCallbackHandler);

		return rowCallbackHandler.getKnjige();
	}
	
	@Transactional
	@Override
	public int save(Knjiga knjiga) {
		PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
			
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				String sql = "INSERT INTO knjiga (naziv, isbn, izdavackaKuca, autor, godinaIzdavanja, opis,"
						+ " slika, cena, brojStranica, tipPoveza, pismo, jezik, brojPrimeraka)"
						+ " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

				PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				int index = 1;
				preparedStatement.setString(index++, knjiga.getNaziv());
				preparedStatement.setLong(index++, knjiga.getIsbn());
				preparedStatement.setString(index++, knjiga.getIzdavackaKuca());
				preparedStatement.setString(index++, knjiga.getAutor());
				preparedStatement.setInt(index++, knjiga.getGodinaIzdavanja());
				preparedStatement.setString(index++, knjiga.getOpis());
				preparedStatement.setString(index++, knjiga.getSlika());
				preparedStatement.setDouble(index++, knjiga.getCena());
				preparedStatement.setInt(index++, knjiga.getBrojStranica());
				preparedStatement.setString(index++, knjiga.getTipPoveza());
				preparedStatement.setString(index++, knjiga.getPismo());
				preparedStatement.setString(index++, knjiga.getJezik());
				preparedStatement.setInt(index++, knjiga.getBrojPrimeraka());
				
				return preparedStatement;
			}

		};
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		boolean uspeh = jdbcTemplate.update(preparedStatementCreator, keyHolder) == 1;
		if (uspeh) {
			String sql = "INSERT INTO knjigaZanr (knjigaId, zanrId) VALUES (?, ?)";
			for (Zanr itZanr: knjiga.getZanrovi()) {	
				uspeh = uspeh && jdbcTemplate.update(sql, keyHolder.getKey(), itZanr.getId()) == 1;
			}
		}
		return uspeh?1:0;
	}
	@Transactional
	@Override
	public int update(Knjiga knjiga) {
		String sql = "DELETE FROM knjigaZanr WHERE knjigaId = ?";
		jdbcTemplate.update(sql, knjiga.getId());
	
		boolean uspeh = true;
		sql = "INSERT INTO knjigaZanr (knjigaId, zanrId) VALUES (?, ?)";
		for (Zanr itZanr: knjiga.getZanrovi()) {	
			uspeh = uspeh &&  jdbcTemplate.update(sql, knjiga.getId(), itZanr.getId()) == 1;
		}

		sql = "UPDATE knjiga SET naziv = ?, isbn = ?, izdavackaKuca = ?, autor = ?, godinaIzdavanja = ?, opis = ?, slika = ?, cena = ?, brojStranica = ?,"
				+ " tipPoveza = ? , pismo = ?, jezik = ?, ocena = ?, brojPrimeraka = ? WHERE id = ?";	
		uspeh = uspeh &&  jdbcTemplate.update(sql, knjiga.getNaziv(), knjiga.getIsbn(), knjiga.getIzdavackaKuca(), knjiga.getAutor(), knjiga.getGodinaIzdavanja(),
				knjiga.getOpis(), knjiga.getSlika(), knjiga.getCena(), knjiga.getBrojStranica(), knjiga.getTipPoveza(), knjiga.getPismo(), knjiga.getJezik(),
				knjiga.getOcena(), knjiga.getBrojPrimeraka(), knjiga.getId()) == 1;
		
		return uspeh?1:0;
	}

	@Override
	public int delete(Long id) {
		String sql = "DELETE FROM knjigaZanr WHERE knjigaId = ?";
		jdbcTemplate.update(sql, id);

		sql = "DELETE FROM knjiga WHERE id = ?";
		return jdbcTemplate.update(sql, id);
	}
	
	private class KnjigaRowMapper implements RowMapper<Knjiga> {

		@Override
		public Knjiga mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			
			Long knjigaId = rs.getLong(index++);
			String knjigaNaziv = rs.getString(index++);
			Long knjigaIsbn = rs.getLong(index++);
			String knjigaIzdavackaKuca = rs.getString(index++);
			String knjigaAutor = rs.getString(index++);
			int knjigaGodinaIzdavanja = rs.getInt(index++);
			String knjigaOpis = rs.getString(index++);
			String knjigaSlika = rs.getString(index++);
			Double knjigaCena = rs.getDouble(index++);
			int knjigaBrojStranica = rs.getInt(index++);
			String knjigaTipPoveza = rs.getString(index++);
			String knjigaPismo = rs.getString(index++);
			String knjigaJezik = rs.getString(index++);
			Double knjigaOcena = rs.getDouble(index++);
			Integer knjigabrojPrimeraka = rs.getInt(index++);

			Knjiga knjiga = new Knjiga(knjigaId, knjigaNaziv, knjigaIsbn, knjigaIzdavackaKuca, knjigaAutor, knjigaGodinaIzdavanja,
					knjigaOpis, knjigaSlika, knjigaCena, knjigaBrojStranica, knjigaTipPoveza, knjigaPismo, knjigaJezik, knjigaOcena,knjigabrojPrimeraka);
			
			return knjiga;
		}

	}

	@Override
	public List<Knjiga> find(String naziv, Long zanrId, Double cenaMin, Double cenaMax, String autor, String jezik, Long isbn) {
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT k.* FROM knjiga k "; 
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(naziv!=null) {
			naziv = "%" + naziv + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.naziv LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(naziv);
		}

		if(cenaMin!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.cena >= ?");
			imaArgumenata = true;
			listaArgumenata.add(cenaMin);
		}
		
		if(cenaMax!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.cena <= ?");
			imaArgumenata = true;
			listaArgumenata.add(cenaMax);
		}
		if(autor!=null) {
			autor = "%" + autor + "%";
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("k.autor LIKE ?");
			imaArgumenata = true;
			listaArgumenata.add(autor);
		}
		
		if(jezik!=null) {
			jezik = "%" + jezik + "%";
			if(imaArgumenata)
			whereSql.append(" AND ");
			whereSql.append("k.jezik LIKE ? ");
			imaArgumenata = true;
			listaArgumenata.add(jezik);
		}
		if(isbn!=null) {
			if(imaArgumenata)
			whereSql.append(" AND ");
			whereSql.append("k.isbn LIKE ? ");
			imaArgumenata = true;
			listaArgumenata.add(isbn);
		}
		
		if(imaArgumenata) {
			sql=sql + whereSql.toString()+"ORDER BY k.id";
		}
		else
			sql=sql + " ORDER BY k.id";
		
		List<Knjiga> knjige = jdbcTemplate.query(sql, listaArgumenata.toArray(), new KnjigaRowMapper());
		for (Knjiga knjiga : knjige) {
			
			knjiga.setZanrovi(findKnjigaZanr(knjiga.getId(), null));
		}

		if(zanrId!=null)
			for (Iterator iterator = knjige.iterator(); iterator.hasNext();) {
				Knjiga knjiga = (Knjiga) iterator.next();
				boolean zaBrisanje = true;
				for (Zanr zanr : knjiga.getZanrovi()) {
					if(zanr.getId() == zanrId) {
						zaBrisanje = false;
						break;
					}
				}
				if(zaBrisanje)
					iterator.remove();
			}
		
		return knjige;
	}
	
	private class KnjigaZanrRowMapper implements RowMapper<Long []> {

		@Override
		public Long [] mapRow(ResultSet rs, int rowNum) throws SQLException {
			int index = 1;
			Long knjigaId = rs.getLong(index++);
			Long zanrId = rs.getLong(index++);

			Long [] knjigaZanr = {knjigaId, zanrId};
			return knjigaZanr;
		}
	}
	
	private List<Zanr> findKnjigaZanr(Long knjigaId, Long zanrId) {
		
		List<Zanr> znaroviKnjige = new ArrayList<Zanr>();
	
		ArrayList<Object> listaArgumenata = new ArrayList<Object>();
		
		String sql = "SELECT kz.knjigaId, kz.zanrId FROM KnjigaZanr kz";
		
		StringBuffer whereSql = new StringBuffer(" WHERE ");
		boolean imaArgumenata = false;
		
		if(knjigaId!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("kz.knjigaId = ?");
			imaArgumenata = true;
			listaArgumenata.add(knjigaId);
		}
		
		if(zanrId!=null) {
			if(imaArgumenata)
				whereSql.append(" AND ");
			whereSql.append("kz.zanrId = ?");
			imaArgumenata = true;
			listaArgumenata.add(zanrId);
		}

		if(imaArgumenata)
			sql=sql + whereSql.toString()+" ORDER BY kz.knjigaId";
		else
			sql=sql + " ORDER BY kz.knjigaId";
		
		List<Long[]> knjigaZanrovi = jdbcTemplate.query(sql, listaArgumenata.toArray(), new KnjigaZanrRowMapper());
				
		for (Long[] kz : knjigaZanrovi) {
			znaroviKnjige.add(zanrDAO.findOne(kz[1]));
		}
		return znaroviKnjige;
	}

}
