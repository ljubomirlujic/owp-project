package com.ftn.OWPLibrary.dao;

import java.util.List;

import com.ftn.OWPLibrary.model.KupljenaKnjiga;


public interface KupljenaKnjigaDAO {

	public KupljenaKnjiga findOne(Long id);

	public List<KupljenaKnjiga> findAll();
	
	public List<KupljenaKnjiga> findAll(Long idKupovine);

	public int save(KupljenaKnjiga knjiga, int kupovinaId);
}
